_start:
    addi r1, r0, 0
    addi r2, r0, 1
    addi r3, r0, 0
    addi r4, r0, 4000000
cycle:
    bl r4, r2, before_print
    beq r4, r2, before_print
    addi r5, r2, 0
    add r2, r1, r2
    addi r1, r5, 0
    addi r5, r0, 2
    rem r6, r2, r5
    addi r5, r0, 1
    beq r5, r6, next_iteration
    add r3, r2, r3
next_iteration:
    jmp cycle
before_print:
    addi r1, r0, 1
    addi r2, r0, 1
    addi r4, r0, 10
count_digits:
    mul r5, r1, r4
    bl r3, r5, print
    beq r3, r5, print
    addi r1, r5, 0
    addi r2, r2, 1
    jmp count_digits
print:
    bl r2, r0, print_newline
    beq r2, r0, print_newline
    div r5, r3, r1
    addi r5, r5, 48
    out r5
    rem r5, r3, r1
    addi r3, r5, 0
    div r5, r1, r4
    addi r1, r5, 0
    subi r2, r2, 1
    jmp print
print_newline:
    hlt